<?php

// Both GET variables must be set, otherwise we cannot process their request.
if(!(isset($_GET['type']) && isset($_GET['number']))){
	?>
	<h2 class="text-center">Request Error</h2>
	<p>There has been an error performing your request due to incomplete request. Refresh the page and try again.</p>
	<button type="button" class="btn btn-block" onClick="back();">Back</button>
	<?php
	die();
}

// Server credentials
$servername = "127.0.0.1"; // 159.65.138.98
$username = "root";
$password = "7f0d0eccd3fe8c265a48c06051f224a362ba13954bc38c21";
$dbname = "LennysRepairs";

// Connect to the server, if fail then print an error and kill connection.
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
	?>
	<h2 class="text-center">Connection failed</h2>
	<p>Error: <?php echo($conn->connect_error); ?></p>
	<button type="button" class="btn btn-block" onClick="back();">Back</button>
	<?php
	die();
}

// Depending on which type of result (repair or receipt) we will use a different SQL command.
if($_GET['type'] == "0"){
	$stmt = $conn->prepare("SELECT * FROM Repairs where Repair_Number= ?"); // To prevent SQL injection, use prepare and bind_param
}else if($_GET['type'] == "1"){
	$stmt = $conn->prepare("SELECT * FROM Repairs where Receipt_Number= ?");
}

// If the field is empty, show error and die.
if($_GET['number'] == ""){
	?>
	<h2 class="text-center">Invalid Input</h2>
	<p>The number field cannot be empty.</p>
	<button type="button" class="btn btn-block" onClick="back();">Back</button>
	<?php
	die();
}

// Put number in the prepared SQL command, this way there is no chance for SQL injection.
$stmt->bind_param("s", $_GET['number']);

// Execute the prepared SQL query.
$stmt->execute();
$result = $stmt->get_result();
$num_of_rows = $result->num_rows;

// Slow down the result to simulate a slow server, so a fancy loading box appears while the results are being "processed."
sleep(1);

// Check if the result is greater than zero, otherwise it does not exist in database.
if ($num_of_rows > 0) {
    while($row = $result->fetch_assoc()) {
		if($_GET['type'] == "0"){ // Status card
			?>
				<h2 class="text-center">Status</h2>
				<?php if($row["Is_Complete"] == "0"){ // Not complete ?>
				<p>Your repairment is still in progress.</p>
				<?php } else if($row["Is_Complete"] == "1"){ // Waiting payment ?>
					<p class="text-center"><button type="button" class="btn btn-call" onClick="loadPageNum(1, '<?php echo($row["Receipt_Number"]);?>');">View Receipt</button></p>
				<?php } else if($row["Is_Complete"] == "2"){ // Complete ?>
					<p>The repairment is complete.</p>
				<?php } ?>
				<p><b>Status:</b> <?php echo($row["Status"]);?></p>
				<p><b>Issue:</b> <?php echo($row["Issue"]);?></p>
				<p><b>Comments:</b><br><?php echo($row["Comments"]);?></p>
			<?php
		}else if($_GET['type'] == "1"){ // Receipt card
			if($row["Is_Complete"] == "1") {
				?>
					<h2 class="text-center">Receipt</h2>
					<p><b>Name:</b> <?php echo($row["Repair_Name"]);?></p>
					<p><b>Repair Number:</b> <?php echo($row["Repair_Number"]);?></p>
					<p><b>Cost:</b> $<?php echo($row["Repair_Cost"]);?> USD</p>
					<p><b>Issue fixed:</b> <?php echo($row["Issue"]);?></p>
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
						<input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="hmksq.ae@hotmail.com">
    <input type="hidden" name="item_name" value="Service Fee">
    <input type="hidden" name="button_subtype" value="services">
    <input type="hidden" name="no_note" value="0">
    <input type="hidden" name="cn" value="Add special instructions to the seller">
    <input type="hidden" name="amount" value="<?php echo($row["Repair_Cost"]);?>">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
	<div class="text-center"><button type="submit" name="submit" class="btn btn-blue">Pay via PayPal <i class="fab fa-paypal"></i></button></div>
				
</form>
				<?php
			} else {
				?>
					<h2 class="text-center">Receipt</h2>
					<p>The payment is already done, visit the status page to see further details of the repairment.</p>
					<div class="text-center"><button type="button" class="btn btn-call" onClick="loadPageNum(0, '<?php echo($row["Repair_Number"]);?>');">View Status</button></div>
				<?php
			}
		}
		?>
		<button type="button" class="btn btn btn-block" onClick="back();">Back</button>
		<?php
		break; // We only want to show one result, there shouldn't be another.
	}
} else { // When there is no result from SQL query.
	?>
	<h2 class="text-center">Not found</h2>
	<p>The number you entered is incorrect.</p>
	<button type="button" class="btn btn btn-block" onClick="back();">Back</button>
	<?php
}

// Close connection
$conn->close();
