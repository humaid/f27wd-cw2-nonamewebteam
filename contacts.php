<!DOCTYPE html>
<html lang="en">
<head>
	<title>Lenny's Pinball World</title>
	<?php include 'base/headlibs.php'; // load head tags froom file ?>
</head>
<body>
	<div class="container">
		<?php include 'base/navbar.php'; // load navbar from file ?>

		<div class="container-mini">
			<div class="card card-grey">

				<h2>Our Email:</h2>
				<a class="btn btn-long" href="mailto:trans_amz@hotmail.com"><i class="fas fa-envelope"></i> trans_amz@hotmail.com</a>
				
				<h2>Our Shipping Address:</h2>

				<p>Lenny's Pinball world
					<br> 1657 QUINN DRIVE
					<br> PLAINFIELD, IL 60586</p>
				<h2>Our Phone Number:</h2>

				<a class="btn btn-call btn-long" href="tel:6302055892"><i class="fas fa-phone" data-fa-transform="flip-h"></i> 630 205 8592</a>

			</div>
		</div>
		<?php include 'base/footer.php'; // load footer from file ?>
	</div>
	</div>
</body>
</html>