# CSS So Far!
Here is documentation about the CSS components we have so far.  
The css code has comments labeling each section.

## Global Page Layout
### Page style
The pages use Arcade font (free) converted for web use, and the page background colour is very light grey (`#f8f8f8`). Also clicked links have yellow outline.  

## Title font
The title "Lenny's Pinball World" uses the `title-font` style to use the Lobster font. The style also adds padding on the left so that the text displays with an indent.  

### Container
The container component help keeps everything on the center of the page, instead of using the full width of the page. Containers could also be used inside of each other. The normal container width is 80%, while `container-mini` width is 70%.  
**Usage:**  
```html
<div class="container">
    <!-- code here -->
</div>
```

## Components
### Navigation bars
Navbars are automatically formatted by the css without adding a `style=` in the tag.  
**Usage:**  
```html
<nav>
    <ul>
        <li>Left of navbar</li>
        <li class="nav-right">Right of navbar</li>
    </ul>
</nav>
```
As you see, if you add the the style `nav-right` on the list item (`<li>`) tag, it floats to the right of the navbar. By default it floats to the left.  

## Cards
Cards are used to hold text and information on a page in a neat way. It currently looks like Bootstrap's jumbotron, it is used by adding the `card` style to a div. Cards use paddings and margins to make elements show properly on the div. Usually cards are placed inside of containers, and cards do not have colours. To add colour you need to use `card-<colour>` styles. Currently, there are grey, green, purple, orange, blue, and red to choose from. Also, for stacking small cards on top of each other without huge paddings and margins, you could add `card-slim`. This is used in the stacked cards on the right side of the homepage.  
**Usage:**  
```html
<div class="container">
    <!-- Title/logo and navigation bar code goes here -->
    <div class="card"> <!-- Transparent card -->
        <p>Welcome to the site!</p>
    </div>
    <div class="card card-grey"> <!-- Grey card -->
        <p>Call us on our phone number!!!! CALL NOW!!!</p>
    </div>
</div>
```
Note: There is a 20px margin between every card, however, slim cards have 10px margin.  

## Buttons
Buttons are partially implemented, all buttons should use the `btn` style at least. The colour of the button is green by default.  
Rounded buttons are also available (for call buttons) by adding the `btn-call` style.  
Colours of the button could be changed, at the time of writing there is only `btn-red` colour available. You can easily add more by copying and pasting the css code and changing the colour field.  
**Usage:**
```html
<button class="btn">A normal green button</button>
<a class="btn btn-call" href="#">A rounded link button</a>
<a class="btn btn-call btn-red" href="/danger-page.php">A nice rounded red button</a>
```

## Flex
We use flexbox for the home page to display the two-third/one-third design. Grid styles must be used inside of a `flex` div. And the number following `grid-<num>` represents the size of the div.  
**Usage:**  
```html
<div class="flex">
    <div class="grid-2">
        <!-- This takes two-thrids --> 
    <div class="grid-1">
        <!-- This takes one-third -->
    </div>
</div>

<!-- Or you can do something like -->

<div class="flex">
    <div class="grid-1">
        <!-- This takes the first half --> 
    <div class="grid-1">
        <!-- This takes the second half -->
    </div>
</div>
```