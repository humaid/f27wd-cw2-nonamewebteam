# no-name-web-team
We fix the Internet, and make it greater than ever! I hope...

## CSS Documentation
I wrote a documentation on [this page](https://github.com/humaidq/no-name-web-team/blob/master/css-so-far.md) which explains the CSS.  


## How files are organised
`base/` folder contains html used in every page, such as header, footer snippits.  
`libraries/` folder contains our own libraries, such as CSS, JS, and the font we use.  

# Licenses
Here we list the licenses for works used on this site.  

Arcade font: https://www.dafont.com/arcade-ya.font  

Arcade image: Sketch by Ibrahim.

## SQL (Hitesh)
SQL code written by Hitesh

```sql
CREATE DATABASE LennysRepairs

USE LennysRepairs

CREATE TABLE Repairs(Repair_Number varchar(16) NOT NULL,
Repair_Name varchar(32) NOT NULL,
Status varchar(32) NOT NULL,
Issue varchar(512) NOT NULL,
Comments varchar(512) NOT NULL,
Is_Complete int(8) NOT NULL,
Receipt_Number varchar(16),
Repair_Cost varchar(16));

INSERT INTO Repairs VALUES ("239-7458", "Chad Wilson", "Ready for pick-up", "Broken paddles need fixing, Ball irresponsive", "27/7: Payment recieved and shipped<br>26/7: Repairments made<br>23/7: Spare parts installed<br>21/7: Ordered spare parts (Ball - cost $17, Paddle(x2) - cost $20)<br>20/7: Found problems stated", 2, "AEF-143-359-0923", "84");
INSERT INTO Repairs VALUES ("807-1632", "Ryan Mayweather", "Waiting for payment", "Ball irresponsive", "22/7:Repairments made<br>22/7: Spare parts installed<br>21/7: Ordered spare parts (Ball - cost $17)<br>19/7: Found problems stated", 1, "AEF-143-549-1144", "45");
INSERT INTO Repairs VALUES ("234-8351", "Michael Hobbs", "Shipped", "Broken pick-up buttons, Scratched glass", "21/7: Payment recieved and shipped (DHL tracking: AE1883924019)<br>18/7:Repairments made<br>17/7: Spare parts installed<br>16/7: Ordered spare parts (Glass sheet - cost $20, Buttons - cost $10)<br>12/7: Found problems stated", 2, "AEF-143-634-2634", "60");
INSERT INTO Repairs VALUES ("626-2533", "Beatrice Gosling", "Ready for pick-up", "Shattered glass, unusable powerups", "31/7: Payment recieved and shipped<br>28/7:Repairments made<br>26/7: Spare parts installed<br>23/7: Ordered spare parts (Glass sheet - cost $20, Powerup icons - cost $50)<br>20/7: Found problems stated", 2, "AEF-143-263-0045", "100");
INSERT INTO Repairs VALUES ("251-7415", "Harry Williams", "Under repair", "Broken paddles need fixing", "30/7: Found problems stated", 0, NULL, NULL);

```
