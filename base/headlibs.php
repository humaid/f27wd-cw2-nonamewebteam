<meta charset="UTF-8">
<meta name="description" content="Welcome to Lenny's Pinball World! We service all types of video and shuffle alley machines. We offer everything from general checkups to full repairs!">
<meta name="keywords" content="Williams,Mills Data East Sega, Stern, Gottlieb, IGT, Bally Pinball, ICE, Raw Thrills,Merit, Force ION, Midway, Capcom">
<meta name="author" content="No-Name Web Team">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
<link rel="stylesheet" type="text/css" href="libraries/global.css">

