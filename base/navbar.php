<div class="flex">
    <div class="grid-2">
        <h1 class="title-font">Lenny's Pinball World</h1>
    </div>
    <div class="grid-1">
        <a class="btn btn-call btn-title" href="tel:6302055892"><i class="fas fa-phone" data-fa-transform="flip-h"></i> 630 205 8592</a>
    </div>
</div>
<nav>
    <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="contacts.php">Contact &amp; Shipping</a></li>
        <li><a href="track.php">Follow Up &amp; Payment</a></li>
        <li class="nav-right"><a target="_blank" href="https://feedback.ebay.com/ws/eBayISAPI.dll?ViewFeedback2&userid=pinballrepair&ftab=AllFeedback">eBay Store <i class="fas fa-external-link-square-alt"></i></a></li>
        <li class="nav-right"><a target="_blank" href="https://rcuae.ae">Donate to Charity <i class="fas fa-external-link-square-alt"></i></a></li>
        
    </ul>
</nav>
