<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lenny's Pinball World</title>
  <?php include 'base/headlibs.php'; // load head tags froom file ?>
</head>
<body>
  <div class="container">
    <?php include 'base/navbar.php'; // load navbar from file ?>

    <div class="flex">
      <div class="grid-2 card card-grey">
        <div class="card-arcade-overlay">
          <p>Welcome to Lenny's Pinball World! We service all types of video and shuffle alley machines. We offer everything
            from general checkups to full repairs, at your home or business location!</p>
          <p>Serving video arcade games and pinball arcades from the 40s and today's.</p>
          <p>Call or email us to book an appointment.</p>
        </div>
      </div>

      <div class="grid-1">
        <div class="card card-slim-top card-grey card-hoverable">
          <h3 class="text-center">We fix:</h3>
        </div>
        <div class="card card-slim card-red card-hoverable">
          <h3>Arcade Machines</h3>
        </div>
        <div class="card card-slim card-blue card-hoverable">
          <h3>Pinball Machines</h3>
        </div>
        <div class="card card-slim card-orange card-hoverable">
          <h3>Shuffle Alley</h3>
        </div>
        <div class="card card-slim card-purple card-hoverable">
          <h3>General checkups &amp; Full service</h3>
        </div>
      </div>
    </div>

    <?php include 'base/footer.php'; // load footer from file ?>
  </div>
</body>
</html>