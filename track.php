<!DOCTYPE html>
<html lang="en">
<head>
    <title>Track Repairment - Lenny's Pinball World</title>
    <?php include 'base/headlibs.php'; // load head tags froom file ?>
</head>
<body>
    <div class="container">
        <?php include 'base/navbar.php'; // load navbar from file ?>
        <div class="container-mini">
            <div class="card card-blue" id="trackDiv">
                <h2 class="text-center">Track your repairment</h2>
                    <p>Tracking Number:</p>
                    <input type="text" id="track"><br>
                    <button type="button" onClick="loadPage(0);" class="btn btn-green btn-block">Check</button>
             </div>
            <div class="card card-red" id="paymentDiv">
                <h2 class="text-center">Pay</h2>
                    <p>Receipt Number:</p>
                    <input type="text" id="receipt"><br>
                    <button type="button" onClick="loadPage(1);" class="btn btn-green btn-block">Go</button>
             </div>
             <div class="card card-red" id="resultDiv"></div>
        </div>
        <?php include 'base/footer.php'; // load footer from file ?>
    </div>
    <script type="text/javascript" src="libraries/trackpage.js"></script>
</body>
</html>