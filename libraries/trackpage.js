// When page loads, hdie the resultDiv/
document.getElementById('resultDiv').style.display = 'none';

// Function to hide the result div.
function back() {
    document.getElementById('resultDiv').style.display = 'none';
    document.getElementById('paymentDiv').style.display = 'block';
    document.getElementById('trackDiv').style.display = 'block';
}

function loadPageNum(type, number) {
    if (type == 0) {
        document.getElementById('track').value = number;
    } else if (type == 1) {
        document.getElementById('receipt').value = number;
    }

    loadPage(type)
}

function loadPage(type) {
    document.getElementById('paymentDiv').style.display = 'none';
    document.getElementById('trackDiv').style.display = 'none';
    document.getElementById('resultDiv').style.display = 'block';
    document.getElementById('resultDiv').innerHTML = "<h2 class=\"text-center\">Loading...</h2>";

    // We will take the tracking or receipt number, depending on type given.
    var number
    if (type == 0) {
        number = document.getElementById('track').value;
    } else if (type == 1) {
        number = document.getElementById('receipt').value;
    }
    document.getElementById('track').value = '';
    document.getElementById('receipt').value = '';

    // Since we cannot really use jQuery, we can use Ajax with XMLHttpRequest without using libraries. We have used a solution we found on the Internet.
    // XHR code modified from StackOverflow answer https://stackoverflow.com/a/17391577
    var ajax;
    try {ajax = new XMLHttpRequest();} catch (e) {alert('Your browser does not support XHR, so the forms will not work. If we use jQuery, this will not be a limitation. Try Firefox/Chrome.');return false;}
    ajax.onreadystatechange = function() {if (ajax.readyState == 4) {document.getElementById('resultDiv').innerHTML = ajax.responseText;}}
    ajax.open("GET", "db_query.php?type=" + type + "&number=" + number, true);
    ajax.send(null);
}